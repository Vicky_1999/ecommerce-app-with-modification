from django.shortcuts import render
from .models.product import Product
from .models.category import Category
# Create your views here.


def index(request):
    category_name = request.GET.get('category', None)
    # print(category_name)
    if category_name:
        category_id = Category.objects.get(name=category_name)
        # print(category_id)
        product = Product.objects.filter(category=category_id)
        # print(product)
    else:
        product = Product.objects.all()
    category = Category.objects.all()
    return render(request, 'index.html', {'Products_list': product, 'Category_list': category})
